# Getting Started with Create React App

## Stack:

-   React
-   Redux
-   SCSS

## ENV

-   `REACT_APP_GET_GENRES_ENDPOINT` - The genres endpoint for music
-   `REACT_APP_GET_EVENTS_ENDPOINT` - The events endpoint
-   `REACT_APP_TICKETMASTER_KEY` - The API key for Ticketmaster
-   `REACT_APP_MUSIC_CLASSIFICATION_ID` - The event type classification (i.e the music classification) ID

### Example

```
REACT_APP_GET_GENRES_ENDPOINT=https://app.ticketmaster.com/discovery/v2/classifications/KZFzniwnSyZfZ7v7nJ
REACT_APP_GET_EVENTS_ENDPOINT=https://app.ticketmaster.com/discovery/v2/events
REACT_APP_TICKETMASTER_KEY=<VALID_API_KEY>
REACT_APP_MUSIC_CLASSIFICATION_ID=KZFzniwnSyZfZ7v7nJ
```
