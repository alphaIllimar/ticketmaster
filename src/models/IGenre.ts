/**
 * Describes a music genre
 */
export interface IGenre {
    /**
     * The ID of the genre.
     * If NULL then shows that all genres should be visible
     */
    id: string | null;
    /**
     * The name of the genre
     */
    name: string;
}
