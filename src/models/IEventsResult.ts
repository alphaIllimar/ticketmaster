import { IEvent } from './IEvent';

/**
 * The return object of the Ticketmaster service
 */
export interface IEventsResult {
    /**
     * The pagination info of the request
     */
    page: IPage;
    /**
     * The events of the request
     */
    events: IEvent[];
}
/**
 * The pagination info regarding the events
 */
export interface IPage {
    /**
     * The number of elements the page has
     */
    size: number;
    /**
     * The total number of events on the page
     */
    totalElements: number;
    /**
     * The total number or pages
     */
    totalPages: number;
    /**
     * The currently loaded in page index
     */
    number: number;
}
