/**
 * Describes a single event on display
 */
export interface IEvent {
    /**
     * The ID of the event
     */
    id: string;
    /**
     * The name of the event
     */
    name: string;
    /**
     * The date/time of the event as an ISO (UTC)
     */
    dateISO: string;
    /**
     * The venue of the event
     */
    venue: IVenue;
    /**
     * The thumbnail of the event for the quick view i.e. event cell
     */
    thumbnailURL: string;
    /**
     * The image of the event for the details
     */
    detailsURL: string;
}
/**
 * Describes the venue of the event
 */
export interface IVenue {
    /**
     * The name of the venue
     */
    name: string;
    /**
     * The city of the venue
     */
    city: string;
    /**
     * The country of the venue
     */
    country: string;
}
