import { createSlice } from '@reduxjs/toolkit';
import { AppThunk, RootState, SliceReducer } from './store';
import { IGenre } from '../models/IGenre';
import { apiCallBegan, IApiCall } from './api';
import { getNameOf } from '../helpers/generalHelpers';
import { AppService } from '../services/AppService';

interface IState {
    /**
     * Holds all the available genres
     */
    genres: IGenre[];
    /**
     * The selected genre's ID or NULL of all should be visible
     */
    selectedGenreId: null | string;
}

const initialState: IState = {
    genres: [],
    selectedGenreId: null,
};

type Reducers = SliceReducer<
    IState,
    {
        /**
         *  The genres of the application are loaded in
         */
        genresAdded: IGenre[];
        /**
         * Allows to set the genre to the specific genre
         */
        setGenre: IGenre;
    }
>;

const slice = createSlice<IState, Reducers>({
    name: 'genre',
    initialState,
    reducers: {
        genresAdded: (state, action) => {
            state.genres = action.payload;
        },
        setGenre: (state, action) => {
            state.selectedGenreId = action.payload.id;
        },
    },
});

export const genreReducer = slice.reducer;
export const { setGenre } = slice.actions;

export const loadAllGenres = (): AppThunk => (dispatch, getState) => {
    const call: IApiCall = {
        method: getNameOf(AppService, (o) => o.getAllGenres),
        onSuccess: slice.actions.genresAdded.type,
    };

    dispatch(apiCallBegan(call));
};

export const allGenres = (store: RootState) => store.genres.genres;
export const selectedGenre = (store: RootState) => store.genres.selectedGenreId;
