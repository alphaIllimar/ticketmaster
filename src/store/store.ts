import {
    configureStore,
    ThunkAction,
    Action,
    PreloadedState,
    Middleware,
    CaseReducer,
    PayloadAction,
} from '@reduxjs/toolkit';
import { mainReducer } from './mainrReducer';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { api } from './middleware/api';
import { error } from './middleware/error';

export type RootState = ReturnType<typeof mainReducer>;

export function setupStore(preloadedState?: PreloadedState<RootState>) {
    return configureStore({
        reducer: mainReducer,
        preloadedState,
        middleware: (getDefaultMiddleware) =>
            getDefaultMiddleware().concat(api, error),
    });
}

export type AppStore = ReturnType<typeof setupStore>;
export const store: AppStore = setupStore();
export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>;
export type AppMiddleware = Middleware<
    {}, // Most middleware do not modify the dispatch return value
    RootState
>;

export type SliceReducer<State, ReducerPayloadMap> = {
    [K in keyof ReducerPayloadMap]: CaseReducer<
        State,
        PayloadAction<ReducerPayloadMap[K]>
    >;
};

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export const useAppDispatch: () => AppDispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
