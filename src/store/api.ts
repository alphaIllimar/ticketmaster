import { createAction } from '@reduxjs/toolkit';

export interface IApiCall {
    /**
     * The method name to be called from the service
     */
    method: string;
    /**
     * The options of the call
     */
    options?: any;
    /**
     * The action to be called on success
     */
    onSuccess?: string;
}

export const apiCallBegan = createAction<IApiCall>('api/callBegan');
export const apiCallSuccess = createAction<any>('api/callSuccess');
export const apiCallFailed = createAction<void>('api/callFailed');
