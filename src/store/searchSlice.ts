import { createSlice } from '@reduxjs/toolkit';
import { RootState, SliceReducer } from './store';

interface IState {
    /**
     * The currently set search input text
     */
    input: string;
    /**
     * The keyword by with the search will be done
     */
    keyword: string | null;
}

const initialState: IState = {
    input: '',
    keyword: null,
};

type Reducers = SliceReducer<
    IState,
    {
        /**
         * Allows to set the input text of the searchbox
         */
        setInput: string;
        /**
         * Allows to set the search keyword of the app
         */
        setKeyword: string | null;
    }
>;

const slice = createSlice<IState, Reducers>({
    name: 'search',
    initialState,
    reducers: {
        setInput: (state, action) => {
            state.input = action.payload;
        },
        setKeyword: (state, action) => {
            state.keyword = action.payload;
        },
    },
});

export const searchReducer = slice.reducer;
export const { setInput, setKeyword } = slice.actions;

export const searchInputText = (store: RootState) => store.search.input;
export const searchKeyword = (store: RootState) => store.search.keyword;
