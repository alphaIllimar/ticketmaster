import { AppService } from '../../services/AppService';
import { IApiCall, apiCallBegan, apiCallFailed, apiCallSuccess } from '../api';
import { AppMiddleware } from '../store';

/**
 * Fetches the data using the Ticketmaster Service
 */
export const api: AppMiddleware = (store) => (next) => async (action) => {
    next(action);
    if (action.type !== apiCallBegan.type) {
        return;
    }

    const call: IApiCall = action.payload;

    try {
        const response = await (AppService as any)[call.method](call.options);

        if (call.onSuccess) {
            store.dispatch({
                type: call.onSuccess,
                payload: response,
            });
        } else {
            store.dispatch({
                type: apiCallSuccess.type,
                payload: response,
            });
        }
    } catch (e) {
        console.error('AppService:ERROR', e);
        store.dispatch({
            type: apiCallFailed.type,
        });
    }
};
