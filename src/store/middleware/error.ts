import { toast } from 'react-toastify';
import { apiCallFailed } from '../api';
import { AppMiddleware } from '../store';
import { phrases } from '../../helpers/phrases';

/**
 * Captures all error events an outputs the error toast
 */
export const error: AppMiddleware = (store) => (next) => async (action) => {
    next(action);
    if (action.type !== apiCallFailed.type) {
        return;
    }

    toast(phrases.apiErrorMessage, {
        type: 'error',
    });
};
