import { createSlice } from '@reduxjs/toolkit';
import { AppThunk, RootState, SliceReducer } from './store';
import { IEvent } from '../models/IEvent';
import { IEventsResult, IPage } from '../models/IEventsResult';
import { apiCallBegan, IApiCall } from './api';
import { getNameOf } from '../helpers/generalHelpers';
import { IGetEventsOptions } from '../services/IAppService';
import { AppService } from '../services/AppService';

const DEFAULT_PAGE_SIZE: number = 50;

interface IState {
    /**
     * Holds all the event currently on display
     */
    events: IEvent[];
    /**
     * The number of events per row
     */
    eventsPerRow: number;
    /**
     * Holds the pagination infor regarding the latest page i.e. data load
     * If NULL no data has been loaded
     */
    page: IPage | null;
    /**
     * The event whom details are currently being shown
     */
    inFocusEvent: IEvent | null;
}

const initialState: IState = {
    events: [],
    eventsPerRow: 0,
    page: null,
    inFocusEvent: null,
};

type Reducers = SliceReducer<
    IState,
    {
        /**
         * Should be called when the API adds in new events
         */
        eventsAdded: IEventsResult;
        /**
         * Should be called when the API appends in new events
         */
        eventsAppended: IEventsResult;
        /**
         * Allows to set the number of event cells per row
         */
        setEventCellsPerRow: number;
        /**
         * Allows you to set the in foucus (details) event
         */
        setInFocusEvent: IEvent | null;
        /**
         * Clears the current events
         */
        clearEvents: void;
    }
>;

const slice = createSlice<IState, Reducers>({
    name: 'events',
    initialState,
    reducers: {
        eventsAdded: (state, action) => {
            state.events = action.payload.events;
            state.page = action.payload.page;
        },
        eventsAppended: (state, action) => {
            state.events = state.events.concat(...action.payload.events);
            state.page = action.payload.page;
        },
        setEventCellsPerRow: (state, action) => {
            state.eventsPerRow = action.payload;
        },
        setInFocusEvent: (state, action) => {
            state.inFocusEvent = action.payload;
        },
        clearEvents: (state) => {
            state.events = [];
            state.inFocusEvent = null;
        },
    },
});

export const eventsReducer = slice.reducer;
export const { setEventCellsPerRow, setInFocusEvent } = slice.actions;

export const loadInNewEvents = (): AppThunk => (dispatch, getState) => {
    const store = getState();
    const options: IGetEventsOptions = {
        keyword: store.search.keyword || '',
        genreID: store.genres.selectedGenreId,
        pageSize: DEFAULT_PAGE_SIZE,
    };

    if (store.events.events.length) {
        dispatch(slice.actions.clearEvents());
    }

    const call: IApiCall = {
        method: getNameOf(AppService, (o) => o.getEvents),
        onSuccess: slice.actions.eventsAdded.type,
        options,
    };

    dispatch(apiCallBegan(call));
};

export const appendInNewEvents = (): AppThunk => (dispatch, getState) => {
    const store = getState();

    const options: IGetEventsOptions = {
        //keyword: store.
        genreID: store.genres.selectedGenreId,
        page: (store.events.page?.number || 0) + 1,
        pageSize: store.events.page?.size,
    };

    if ((options.page || 0) < (store.events.page?.totalPages || 0)) {
        const call: IApiCall = {
            method: getNameOf(AppService, (o) => o.getEvents),
            onSuccess: slice.actions.eventsAppended.type,
            options,
        };

        dispatch(apiCallBegan(call));
    }
};

export const eventsOnDisplay = (store: RootState) => store.events.events;
export const eventsPage = (store: RootState) => store.events.page;
export const eventsPerRow = (store: RootState) => store.events.eventsPerRow;
export const inFocusEvent = (store: RootState) => store.events.inFocusEvent;
