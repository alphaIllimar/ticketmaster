import { createSlice } from '@reduxjs/toolkit';
import { RootState, SliceReducer } from './store';

/**
 * Shows with page is currently visible
 */
export enum Page {
    /**
     * The events display page
     */
    Events,
    /**
     * The all genres display page
     */
    Genres,
}

interface IState {
    /**
     * Shows with page the user is currently on
     */
    page: Page;
}

const initialState: IState = {
    page: Page.Events,
};

type Reducers = SliceReducer<
    IState,
    {
        /**
         *  Allows you to set the spesific page of the app
         */
        setPage: Page;
    }
>;

const slice = createSlice<IState, Reducers>({
    name: 'general',
    initialState,
    reducers: {
        setPage: (state, action) => {
            state.page = action.payload;
        },
    },
});

export const generalReducer = slice.reducer;
export const { setPage } = slice.actions;

export const selectedPage = (store: RootState) => store.general.page;
