import { combineReducers } from '@reduxjs/toolkit';
import { generalReducer } from './generalSlice';
import { eventsReducer } from './eventsSlice';
import { genreReducer } from './genreSlice';
import { searchReducer } from './searchSlice';

export const mainReducer = combineReducers({
    general: generalReducer,
    events: eventsReducer,
    genres: genreReducer,
    search: searchReducer,
});
