import { IGenre } from '../../models/IGenre';
import { allGenres } from '../../store/genreSlice';
import { useAppSelector } from '../../store/store';
import { GenreLink } from '../GenreLink/GenreLink';
import styles from './AllGenres.module.scss';

export function AllGenres() {
    const _allGenres: IGenre[] = useAppSelector(allGenres);

    return (
        <div className={styles.container}>
            <ul>
                {_allGenres.map((genre: IGenre) => (
                    <li key={genre.id}>
                        <GenreLink genre={genre} />
                    </li>
                ))}
            </ul>
        </div>
    );
}
