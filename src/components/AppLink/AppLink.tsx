import { IAppLinkProps } from './IAppLinkProps';
import styles from './AppLink.module.scss';

export function AppLink(props: IAppLinkProps) {
    return (
        <a
            href="#"
            onClick={() => props.onClick?.()}
            className={`${styles.link} ${props.isActive ? styles.active : ''}`}
        >
            {props.name}
        </a>
    );
}
