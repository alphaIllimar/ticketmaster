export interface IAppLinkProps {
    /**
     * Allows to use a custom click
     */
    onClick?: () => void;
    /**
     * The display name of the link
     */
    name: string;
    /**
     * If TRUE the link will be active
     */
    isActive?: boolean;
}
