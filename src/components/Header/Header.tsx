import styles from './Header.module.scss';

import { phrases } from '../../helpers/phrases';
import { Search } from '../Search/Search';
import { Genres } from '../Genres/Genres';

export function Header() {
    return (
        <header>
            <div className={styles.top}>
                <h1>{phrases.heading}</h1>
                <div className={styles.search}>
                    <Search />
                </div>
            </div>
            <Genres />
        </header>
    );
}
