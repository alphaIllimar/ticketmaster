import { IEvent } from '../../models/IEvent';

export interface IEventDetailsProps {
    /**
     * The event for whom you want to show the details
     */
    event: IEvent;
}
