import { phrases } from '../../helpers/phrases';
import { IEventDetailsProps } from './IEventDetailsProps';
import styles from './EventDetails.module.scss';
import { useAppDispatch, useAppSelector } from '../../store/store';
import {
    setInFocusEvent,
    eventsOnDisplay,
    eventsPerRow,
} from '../../store/eventsSlice';
import { IEvent } from '../../models/IEvent';
import { Format } from '../../helpers/Format';

export function EventDetails(props: IEventDetailsProps) {
    const _dispatch = useAppDispatch();
    const _events = useAppSelector(eventsOnDisplay);
    const _eventsPerRow = useAppSelector(eventsPerRow);

    const _index: number = _events.findIndex(
        (e: IEvent) => e.id === props.event.id
    );
    const _row: number = Math.ceil((_index + 1) / _eventsPerRow) + 1;
    const _cellPer: number = 100 / _eventsPerRow;
    const _arrowPosition: number =
        (_index % _eventsPerRow) * _cellPer + _cellPer / 2;

    //composes the venue text
    const _venue: React.ReactNode[] = [];
    if (props.event.venue.name) {
        _venue.push(
            <span className={styles.venueName}>{props.event.venue.name}</span>
        );
    }
    if (props.event.venue.city) {
        if (_venue.length) {
            _venue.push(', ');
        }
        _venue.push(<span>{props.event.venue.city}</span>);
    }
    if (props.event.venue.country) {
        if (_venue.length) {
            _venue.push(', ');
        }
        _venue.push(<span>{props.event.venue.country}</span>);
    }

    return (
        <div
            className={styles.container}
            style={{
                gridColumn: `1 / span ${_eventsPerRow}`,
                gridRow: `${_row}`,
            }}
        >
            <div className={styles.arrowContainer}>
                <div
                    className={styles.arrow}
                    style={{
                        left: `${_arrowPosition}%`,
                    }}
                ></div>
            </div>
            <div className={styles.contentContainer}>
                <div className={styles.details}>
                    <h3>{props.event.name}</h3>
                    <p className={styles.date}>
                        {!!props.event.dateISO &&
                            `${Format.isoWeekday(
                                props.event.dateISO,
                                'en-US'
                            )}, ${Format.isoDate(
                                props.event.dateISO
                            )} @ ${Format.isoTime(props.event.dateISO)}`}
                    </p>
                    <p className={styles.venue}>
                        {_venue.map((node, index) => (
                            <span key={index}>{node}</span>
                        ))}
                    </p>
                    <p className={styles.content}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.
                    </p>
                    <div className={styles.buttons}>
                        <button
                            onClick={() => _dispatch(setInFocusEvent(null))}
                        >
                            {phrases.closeDetails}
                        </button>
                    </div>
                </div>
                <div
                    style={{
                        backgroundImage: `url('${props.event.detailsURL}')`,
                    }}
                    className={styles.image}
                ></div>
            </div>
        </div>
    );
}
