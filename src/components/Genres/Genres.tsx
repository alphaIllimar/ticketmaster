import styles from './Genres.module.scss';

import { phrases } from '../../helpers/phrases';
import { HiddenText } from '../HiddenText/HiddenText';
import { useAppDispatch, useAppSelector } from '../../store/store';
import React from 'react';
import { allGenres, loadAllGenres } from '../../store/genreSlice';
import { IGenre } from '../../models/IGenre';
import { GenreLink } from '../GenreLink/GenreLink';
import { AppLink } from '../AppLink/AppLink';
import { Page, selectedPage, setPage } from '../../store/generalSlice';

/**
 * The number of genres visible on the site
 */
const NR_VISIBLE: number = 3;

export function Genres() {
    const _dispatch = useAppDispatch();
    const _allGenres: IGenre[] = useAppSelector(allGenres);
    const _page: Page = useAppSelector(selectedPage);

    React.useEffect(() => {
        _dispatch(loadAllGenres());
    }, [_dispatch]);

    return (
        <div className={styles.container}>
            <HiddenText>
                <h2>{phrases.genres}</h2>
            </HiddenText>
            <ul>
                <li>
                    <GenreLink
                        genre={{
                            name: phrases.allGenres,
                            id: null,
                        }}
                    />
                </li>
                {_allGenres.slice(0, NR_VISIBLE).map((genre: IGenre) => (
                    <li key={genre.id}>
                        <GenreLink genre={genre} />
                    </li>
                ))}
                {_allGenres.length > NR_VISIBLE && (
                    <li>
                        <AppLink
                            name={phrases.more}
                            onClick={() => {
                                _dispatch(setPage(Page.Genres));
                            }}
                            isActive={_page === Page.Genres}
                        />
                    </li>
                )}
            </ul>
        </div>
    );
}
