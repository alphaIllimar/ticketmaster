import { IGenre } from '../../models/IGenre';

export interface IGenreLinkProps {
    /**
     * The genre whom the link is for
     */
    genre: IGenre;
    /**
     * Allows to use a custom click
     */
    onClick?: () => void;
}
