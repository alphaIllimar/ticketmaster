import { loadInNewEvents } from '../../store/eventsSlice';
import { selectedGenre, setGenre } from '../../store/genreSlice';
import { useAppDispatch, useAppSelector } from '../../store/store';
import { IGenreLinkProps } from './IGenreLinkProps';
import { AppLink } from '../AppLink/AppLink';
import { Page, selectedPage, setPage } from '../../store/generalSlice';
import { setInput, setKeyword } from '../../store/searchSlice';

export function GenreLink(props: IGenreLinkProps) {
    const _dispatch = useAppDispatch();
    const _selectedGenreId: string | null = useAppSelector(selectedGenre);
    const _page: Page = useAppSelector(selectedPage);

    return (
        <AppLink
            name={props.genre.name}
            onClick={() => {
                _dispatch(setGenre(props.genre));
                _dispatch(setPage(Page.Events));
                _dispatch(setInput(''));
                _dispatch(setKeyword(null));
                _dispatch(loadInNewEvents());
            }}
            isActive={
                _page === Page.Events && _selectedGenreId === props.genre.id
            }
        />
    );
}
