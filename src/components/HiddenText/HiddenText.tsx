import * as React from 'react';

export function HiddenText(props: React.PropsWithChildren<{}>) {
    return (
        <span
            style={{
                position: 'fixed',
                top: -1000,
                left: -1000,
                width: 0,
                height: 0,
                overflow: 'hidden',
            }}
        >
            {props.children}
        </span>
    );
}
