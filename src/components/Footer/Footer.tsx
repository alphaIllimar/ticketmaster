import { phrases } from '../../helpers/phrases';
import styles from './Footer.module.css';

export function Footer() {
    return (
        <footer className={styles.container}>
            <p>{phrases.footerText}</p>
        </footer>
    );
}
