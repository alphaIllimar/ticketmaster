export interface ISearchBoxProps {
    /**
     * The placeholder text used for the search box
     */
    placeholder?: string;
    /**
     * The method that will be called when the box auto searched on typing
     * @param pharse The pharse to be searched
     */
    onAutoSearch?: (pharse: string) => void;
    /**
     * The method that will be called when the search button is clicked
     * @param pharse The pharse to be searched
     */
    onSearchButtonClick?: (pharse: string) => void;
    /**
     * The method that will be called when the search input changes
     * @param value The value in the input box
     */
    onInputChange: (value: string) => void;
    /**
     * The value of the search input box
     */
    value: string;
    /**
     * The hidden text for the search button
     */
    searchButtonText: string;
}
