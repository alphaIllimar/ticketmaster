import styles from './SearchBox.module.scss';
import { HiddenText } from '../HiddenText/HiddenText';
import React from 'react';
import { ISearchBoxProps } from './ISearchBoxProps';

export function SearchBox(props: ISearchBoxProps) {
    const _inputRef = React.useRef<HTMLInputElement>(null);
    const _timeoutRef = React.useRef<any>(null);

    return (
        <div className={styles.container}>
            <input
                placeholder={props.placeholder || ''}
                value={props.value || ''}
                onChange={(e) => {
                    const searchText: string = e.target.value;
                    props.onInputChange?.(searchText);

                    clearTimeout(_timeoutRef.current);
                    _timeoutRef.current = setTimeout(() => {
                        props.onAutoSearch?.(searchText);
                    }, 300);
                }}
                ref={_inputRef}
            />
            <button
                onClick={() => {
                    if (!props.value) {
                        _inputRef.current?.focus();
                    } else {
                        props.onSearchButtonClick?.(props.value);
                    }
                }}
            >
                <HiddenText>
                    <span>{props.searchButtonText}</span>
                </HiddenText>
            </button>
        </div>
    );
}
