import { act, cleanup, screen } from '@testing-library/react';
import { setAppService } from '../../services/AppService';
import { DummyService } from '../../services/DummyService';
import { dummyStore, renderWithProviders } from '../../helpers/testHelper';
import { Events } from './Events';
import { phrases } from '../../helpers/phrases';
import { setupStore } from '../../store/store';
import userEvent from '@testing-library/user-event';

afterEach(() => {
    cleanup();
});
beforeEach(() => {
    setAppService(new DummyService());
});

it('tests events layout rendering and the details opening/closing', async () => {
    const observe = jest.fn();
    const unobserve = jest.fn();

    // you can also pass the mock implementation
    // to jest.fn as an argument
    (window as any).IntersectionObserver = jest.fn(() => ({
        observe,
        unobserve,
    }));

    renderWithProviders(<Events />, {
        store: setupStore(dummyStore),
    });

    //checks that the events rendred
    expect(screen.getByText(phrases.events)).toBeInTheDocument();

    expect(observe).toBeCalled();

    const events = screen.getAllByRole('button'); //(styles.event);
    expect(events.length === 20).toBeTruthy();

    const firstEventBtn = screen.getByText(dummyStore.events.events[0].name);

    act(() => {
        userEvent.click(firstEventBtn);
    });

    const detailsCloseBtn = screen.getByText(phrases.closeDetails);
    expect(detailsCloseBtn).toBeInTheDocument();

    act(() => {
        userEvent.click(detailsCloseBtn);
    });

    expect(detailsCloseBtn).not.toBeInTheDocument();
});
