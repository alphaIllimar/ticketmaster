import styles from './Events.module.scss';

import { phrases } from '../../helpers/phrases';
import { HiddenText } from '../HiddenText/HiddenText';
import { useAppDispatch, useAppSelector } from '../../store/store';
import React from 'react';
import {
    setEventCellsPerRow,
    setInFocusEvent,
    appendInNewEvents,
    eventsOnDisplay,
    eventsPage,
    inFocusEvent,
} from '../../store/eventsSlice';
import { IEvent } from '../../models/IEvent';
import { IPage } from '../../models/IEventsResult';
import { EventDetails } from '../EventDetails/EventDetails';

export function Events() {
    const _dispatch = useAppDispatch();
    const _displayedEvents: IEvent[] = useAppSelector(eventsOnDisplay);
    const _moreDataRef = React.useRef<Element>();
    const _page: IPage | null = useAppSelector(eventsPage);
    const _cellWidthRef = React.useRef<HTMLDivElement>(null);
    const _eventsAreaRef = React.useRef<HTMLDivElement>(null);
    const _inFoucusEvent = useAppSelector(inFocusEvent);

    React.useEffect(() => {
        const fnSetEventsPerRow = (): void => {
            const cellsPerRow = Math.round(
                (_eventsAreaRef.current?.offsetWidth || 0) /
                    (_cellWidthRef.current?.offsetWidth || 0)
            );
            _dispatch(setEventCellsPerRow(cellsPerRow));
        };

        fnSetEventsPerRow();
        window.addEventListener('resize', fnSetEventsPerRow);

        return () => {
            window.removeEventListener('resize', fnSetEventsPerRow);
        };
    }, [_dispatch]);

    React.useEffect(() => {
        const observer = new IntersectionObserver((entities) => {
            const entity = entities[0];
            if (entity.isIntersecting) {
                _dispatch(appendInNewEvents());
                observer.unobserve(_moreDataRef.current!);
            }
        });
        if (_moreDataRef.current) {
            observer.observe(_moreDataRef.current);
        }

        return () => {
            if (_moreDataRef.current) {
                observer.unobserve(_moreDataRef.current!);
            }
        };
    }, [_displayedEvents, _dispatch]);

    return (
        <div className={styles.container}>
            <HiddenText>
                <h2>{phrases.events}</h2>
            </HiddenText>
            <div
                style={{
                    height: 0,
                    overflow: 'hidden',
                }}
            >
                <div className={styles.events}>
                    <div ref={_cellWidthRef}></div>
                </div>
            </div>
            <div className={styles.events} ref={_eventsAreaRef}>
                {_displayedEvents.map((event: IEvent, index: number) => {
                    const btn: React.ReactNode = (
                        <button
                            onClick={() => {
                                _dispatch(
                                    setInFocusEvent(
                                        _inFoucusEvent?.id === event.id
                                            ? null
                                            : event
                                    )
                                );
                            }}
                            style={{
                                backgroundImage: `url('${event.thumbnailURL}')`,
                            }}
                        >
                            <HiddenText>{event.name}</HiddenText>
                        </button>
                    );
                    if (
                        (_page?.totalPages || 0) - 1 > (_page?.number || 0) &&
                        index === _displayedEvents.length - 1
                    ) {
                        return (
                            <div
                                className={styles.event}
                                ref={(elm) => {
                                    if (elm) {
                                        _moreDataRef.current = elm;
                                    }
                                }}
                                key={event.id}
                            >
                                {btn}
                            </div>
                        );
                    } else {
                        return (
                            <div className={styles.event} key={event.id}>
                                {btn}
                            </div>
                        );
                    }
                })}
                {!!_inFoucusEvent && <EventDetails event={_inFoucusEvent} />}
            </div>
        </div>
    );
}
