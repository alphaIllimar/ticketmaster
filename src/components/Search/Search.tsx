import { phrases } from '../../helpers/phrases';
import { HiddenText } from '../HiddenText/HiddenText';
import { useAppDispatch, useAppSelector } from '../../store/store';
import { searchInputText, setInput, setKeyword } from '../../store/searchSlice';
import { loadInNewEvents } from '../../store/eventsSlice';
import { setGenre } from '../../store/genreSlice';
import { Page, setPage } from '../../store/generalSlice';
import { SearchBox } from '../SearchBox/SearchBox';

export function Search() {
    const _dispatch = useAppDispatch();
    const _inputText: string = useAppSelector(searchInputText);

    const _doSearch = (searchPhrase: string): void => {
        _dispatch(setKeyword(searchPhrase));
        _dispatch(
            setGenre({
                id: null,
                name: '',
            })
        );
        _dispatch(setPage(Page.Events));
        _dispatch(loadInNewEvents());
    };

    return (
        <div>
            <HiddenText>
                <h2>{phrases.search}</h2>
            </HiddenText>
            <SearchBox
                placeholder={phrases.searchPlaceHolder}
                value={_inputText}
                onInputChange={(value: string) => _dispatch(setInput(value))}
                onAutoSearch={_doSearch}
                onSearchButtonClick={_doSearch}
                searchButtonText={phrases.search}
            />
        </div>
    );
}
