import './App.module.scss';
import styles from './App.module.scss';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { Events } from './components/Events/Events';
import { Footer } from './components/Footer/Footer';
import { Header } from './components/Header/Header';
import React from 'react';
import { useAppDispatch, useAppSelector } from './store/store';
import { loadInNewEvents } from './store/eventsSlice';
import { AllGenres } from './components/AllGenres/AllGenres';
import { Page, selectedPage } from './store/generalSlice';

export function App() {
    const _dispatch = useAppDispatch();
    const _page: Page = useAppSelector(selectedPage);

    React.useEffect(() => {
        _dispatch(loadInNewEvents());
    }, [_dispatch]);

    return (
        <>
            <div className={styles.container}>
                <Header />
                <main>
                    {_page === Page.Events && <Events />}
                    {_page === Page.Genres && <AllGenres />}
                </main>
                <Footer />
            </div>
            <ToastContainer />
        </>
    );
}
