/**
 * Holds all the translations of the application
 */
export const phrases = {
    heading: 'Music events',
    search: 'Search',
    searchPlaceHolder: 'Search for events...',
    allGenres: 'All genres',
    genres: 'Genres',
    events: 'Events',
    footerText: 'Fitek 2020',
    more: 'More...',
    apiErrorMessage: 'Ups... something went wrong!',
    closeDetails: 'Close details',
};
