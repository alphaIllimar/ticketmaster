/**
 * Does a millisecond delay for a async function
 * @param ms The milliseconds you want to delay
 */
export const delay = (ms: number): Promise<void> =>
    new Promise((res) => setTimeout(res, ms));
/**
 * Collects all the object property names on a requsive mannor
 * @param obj The object to be examened
 * @param ret The return array to with the names will be appended
 * @returns An array of all the property names prototyped in
 */
function _getAllPrototypeNames(obj: object, ret: string[] = []): string[] {
    if (obj == null) {
        return ret;
    }
    ret = ret.concat(...Object.getOwnPropertyNames(obj));
    return _getAllPrototypeNames(Object.getPrototypeOf(obj), ret);
}
/**
 * The response type for the getNameOf function
 */
type GetNameOfResponseObjet<T> = {
    [key in keyof T]: string;
};
/**
 * Gets the name of the object member as a string
 * @param obj The object whom member you seek
 * @param expression The method that will select the member from the object
 * @returns The name of the member as a string
 */
export function getNameOf<T>(
    obj: T,
    expression: (x: GetNameOfResponseObjet<T>) => string
): keyof T {
    let response: GetNameOfResponseObjet<T> = {} as GetNameOfResponseObjet<T>;
    Object.getOwnPropertyNames(obj).forEach((key: string) => {
        response[key as keyof T] = key;
    });

    //if the property in question was not in the first layer
    if (!expression(response)) {
        _getAllPrototypeNames(obj as object).forEach((key: string) => {
            response[key as keyof T] = key;
        });
    }

    return expression(response) as keyof T;
}
