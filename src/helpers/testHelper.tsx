import React, { PropsWithChildren } from 'react';
import { render } from '@testing-library/react';
import type { RenderOptions } from '@testing-library/react';
import type { PreloadedState } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';

import { setupStore } from '../store/store';
import type { AppStore, RootState } from '../store/store';

// This type interface extends the default options for render from RTL, as well
// as allows the user to specify other things such as initialState, store.
interface ExtendedRenderOptions extends Omit<RenderOptions, 'queries'> {
    preloadedState?: PreloadedState<RootState>;
    store?: AppStore;
}

export function renderWithProviders(
    ui: React.ReactElement,
    {
        preloadedState = {},
        // Automatically create a store instance if no store was passed in
        store = setupStore(preloadedState),
        ...renderOptions
    }: ExtendedRenderOptions = {}
) {
    function Wrapper({ children }: PropsWithChildren<{}>): JSX.Element {
        return <Provider store={store}>{children}</Provider>;
    }
    return {
        store,
        ...render(ui, { wrapper: Wrapper, ...renderOptions }),
    };
}

export const dummyStore: RootState = {
    general: {
        page: 0,
    },
    events: {
        events: [
            {
                id: 'Z698xZ8KZ17GPug',
                name: 'HPO : Bright Classics',
                dateISO: '2023-10-26T16:00:00Z',
                venue: {
                    name: 'Musiikkitalo, Concert hall',
                    city: 'Helsinki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GY0K',
                name: "Ahjo Ensemble's 10th anniversary concert",
                dateISO: '2023-10-27T17:00:19Z',
                venue: {
                    name: '',
                    city: 'Helsinki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/a/cd4/f6cd56c9-99c5-40d3-887f-400def6b8cd4_1545921_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/a/cd4/f6cd56c9-99c5-40d3-887f-400def6b8cd4_1545921_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GsF9',
                name: 'Ysärin Kuninkaalliset - kiertue 2019 / SEINÄJOKI',
                venue: {
                    name: '',
                    city: 'Seinäjoki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/a/481/086e0db8-6fee-4540-93ce-4369f4802481_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/a/481/086e0db8-6fee-4540-93ce-4369f4802481_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GsqM',
                name: 'Glenda - musiikkinäytelmä rakkauden tositarinoista',
                dateISO: '2023-10-27T16:00:00Z',
                venue: {
                    name: '',
                    city: 'Helsinki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/a/944/d825c1ce-70b6-4d35-9841-ed5d24ddb944_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/a/944/d825c1ce-70b6-4d35-9841-ed5d24ddb944_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GwAd',
                name: 'SIBA: NYKY Ensemble x Korvat Auki',
                dateISO: '2023-10-27T16:00:00Z',
                venue: {
                    name: '',
                    city: 'Helsinki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/a/e48/a1485daa-fa8d-4988-a163-5f3f37132e48_1282981_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/a/e48/a1485daa-fa8d-4988-a163-5f3f37132e48_1282981_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GJpK',
                name: 'VVO: Samuli Edelmann - Vaiheet',
                dateISO: '2023-10-28T16:00:00Z',
                venue: {
                    name: 'Musiikkitalo, Concert hall',
                    city: 'Helsinki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/a/f14/61bedb50-be43-4efd-8e84-0303fd51df14_1396441_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17Gs-K',
                name: 'HKO - Horizon Series Gülistan Ensemble',
                dateISO: '2023-10-28T15:00:00Z',
                venue: {
                    name: '',
                    city: 'Helsinki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GsF_',
                name: 'Ysärin Kuninkaalliset - kiertue 2019 / SEINÄJOKI',
                venue: {
                    name: '',
                    city: 'Seinäjoki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/c/80b/f3cd8d24-c3ae-4fa0-b4bc-1ba99f9b380b_106091_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/c/80b/f3cd8d24-c3ae-4fa0-b4bc-1ba99f9b380b_106091_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17Gwuy',
                name: 'SIBA: Charles Quevillon: Electric Unconscious',
                dateISO: '2023-10-31T17:00:00Z',
                venue: {
                    name: '',
                    city: 'Helsinki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/a/e48/a1485daa-fa8d-4988-a163-5f3f37132e48_1282981_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/a/e48/a1485daa-fa8d-4988-a163-5f3f37132e48_1282981_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GEas',
                name: 'OMJ Jazz & Etno: Tuomas Paukku Scription',
                dateISO: '2023-11-01T17:00:00Z',
                venue: {
                    name: '',
                    city: 'Oulu',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/a/b5f/a96fa8aa-e215-4706-a261-cb4431f43b5f_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/a/b5f/a96fa8aa-e215-4706-a261-cb4431f43b5f_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GJ8-',
                name: "Oulu Sinfonia: Brahm's Final Works",
                dateISO: '2023-11-02T17:00:00Z',
                venue: {
                    name: '',
                    city: 'Oulu',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/a/ebc/c382c09a-7325-48f1-a0fe-3789f64c1ebc_1566241_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/a/ebc/c382c09a-7325-48f1-a0fe-3789f64c1ebc_1566241_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GE1o',
                name: 'Stam1na, Diablo, Corroded,  Dear Mother',
                dateISO: '2023-11-03T15:30:00Z',
                venue: {
                    name: '',
                    city: 'Helsinki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/a/6f3/eb2177fd-c19a-4632-b89d-9059133236f3_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/a/6f3/eb2177fd-c19a-4632-b89d-9059133236f3_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GP8M',
                name: 'HPO : Tales of the Night',
                dateISO: '2023-11-03T17:00:00Z',
                venue: {
                    name: 'Musiikkitalo, Concert hall',
                    city: 'Helsinki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GPoA',
                name: 'HPO : Tales of the Night',
                dateISO: '2023-11-03T11:00:00Z',
                venue: {
                    name: 'Musiikkitalo, Concert hall',
                    city: 'Helsinki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GYFN',
                name: 'Gettomasa + Kube ja Vitun Iso Rotsi',
                venue: {
                    name: '',
                    city: 'Kempele',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/c/80b/f3cd8d24-c3ae-4fa0-b4bc-1ba99f9b380b_106091_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/c/80b/f3cd8d24-c3ae-4fa0-b4bc-1ba99f9b380b_106091_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17Gw8d',
                name: 'SiBA: Maisterikonsertti: Jere Hölttä, laulu',
                dateISO: '2023-11-03T15:00:00Z',
                venue: {
                    name: '',
                    city: 'Helsinki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/a/e48/a1485daa-fa8d-4988-a163-5f3f37132e48_1282981_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/a/e48/a1485daa-fa8d-4988-a163-5f3f37132e48_1282981_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GEuS',
                name: 'Kummeli-Show - Ensimmäinen Jäähyväiskiertue',
                venue: {
                    name: '',
                    city: 'Kempele',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/a/2f1/b54dd0a4-5abb-4a0b-93bb-61ef82f622f1_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/a/2f1/b54dd0a4-5abb-4a0b-93bb-61ef82f622f1_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17Gs89',
                name: 'Legendaarinen vappukonsertti',
                dateISO: '2023-11-04T13:00:00Z',
                venue: {
                    name: 'Musiikkitalo, Concert hall',
                    city: 'Helsinki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/a/cd4/f6cd56c9-99c5-40d3-887f-400def6b8cd4_1545921_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/a/cd4/f6cd56c9-99c5-40d3-887f-400def6b8cd4_1545921_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GEaS',
                name: 'OMJ Jazz & Etno: 5/5',
                dateISO: '2023-11-07T17:00:00Z',
                venue: {
                    name: '',
                    city: 'Oulu',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/a/bca/8e7c0213-67db-450e-9c27-03d3e63dabca_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/a/bca/8e7c0213-67db-450e-9c27-03d3e63dabca_TABLET_LANDSCAPE_16_9.jpg',
            },
            {
                id: 'Z698xZ8KZ17GPuP',
                name: 'HPO : Towards the Sky',
                dateISO: '2023-11-08T17:00:00Z',
                venue: {
                    name: 'Musiikkitalo, Concert hall',
                    city: 'Helsinki',
                    country: 'Finland',
                },
                thumbnailURL:
                    'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_RETINA_PORTRAIT_3_2.jpg',
                detailsURL:
                    'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_TABLET_LANDSCAPE_16_9.jpg',
            },
        ],
        eventsPerRow: 4,
        page: {
            size: 20,
            totalElements: 111,
            totalPages: 2,
            number: 0,
        },
        inFocusEvent: null,
    },
    genres: {
        genres: [],
        selectedGenreId: null,
    },
    search: {
        input: '',
        keyword: null,
    },
} as any;
