import { delay } from '../helpers/generalHelpers';
import { IEventsResult } from '../models/IEventsResult';
import { IGenre } from '../models/IGenre';
import { IAppService, IGetEventsOptions } from './IAppService';

export class DummyService implements IAppService {
    public async getAllGenres(): Promise<IGenre[]> {
        throw 'DummyService.getAllGenres() - Not Implemented';
    }

    public async getEvents(options: IGetEventsOptions): Promise<IEventsResult> {
        if (options.keyword || options.page !== 1 || options.genreID) {
            throw 'DummyService.getEvents() - Call not Implemented';
        }

        await delay(200);

        return {
            events: [
                {
                    id: 'Z698xZ8KZ17GYoK',
                    name: 'HKO: Final rehearsals From the New World',
                    dateISO: '2023-11-08T08:00:00Z',
                    venue: {
                        name: 'Musiikkitalo, Concert hall',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17Gwv_',
                    name: 'SIBA: Aili Järvelä',
                    dateISO: '2023-11-08T17:00:00Z',
                    venue: {
                        name: 'Kalevan Navetta, Hugo-Sali',
                        city: 'Seinäjoki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/e48/a1485daa-fa8d-4988-a163-5f3f37132e48_1282981_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/a/e48/a1485daa-fa8d-4988-a163-5f3f37132e48_1282981_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GJ8N',
                    name: 'Oulu Sinfonia: Portrait of a Dictator',
                    dateISO: '2023-11-09T17:00:00Z',
                    venue: {
                        name: '',
                        city: 'Oulu',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/ebc/c382c09a-7325-48f1-a0fe-3789f64c1ebc_1566241_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/a/ebc/c382c09a-7325-48f1-a0fe-3789f64c1ebc_1566241_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GPCK',
                    name: 'HPO: Towards the Sky',
                    dateISO: '2023-11-09T17:00:00Z',
                    venue: {
                        name: 'Musiikkitalo, Concert hall',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GtOK',
                    name: 'LORNA SHORE - THE PAIN REMAINS EU TOUR 2023',
                    dateISO: '2023-11-09T15:30:00Z',
                    venue: {
                        name: 'HELSINKI ICEHALL',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/242/263dd529-e792-4aca-8a51-e07dee4e7242_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/a/242/263dd529-e792-4aca-8a51-e07dee4e7242_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GJp4',
                    name: 'VVO: We Can Be Heroes',
                    dateISO: '2023-11-11T17:00:00Z',
                    venue: {
                        name: 'Musiikkitalo, Concert hall',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/f14/61bedb50-be43-4efd-8e84-0303fd51df14_1396441_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17Gwug',
                    name: 'Helsinki Folk Party',
                    dateISO: '2023-11-11T16:00:00Z',
                    venue: {
                        name: '',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/cd4/f6cd56c9-99c5-40d3-887f-400def6b8cd4_1545921_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/a/cd4/f6cd56c9-99c5-40d3-887f-400def6b8cd4_1545921_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GJ-K',
                    name: 'OKEAN ELZY - HELP FOR UKRAINE TOUR 2023',
                    dateISO: '2023-11-12T18:00:00Z',
                    venue: {
                        name: '',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/281/35bed69b-a4f0-419e-9ad8-6bca35fc7281_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/a/281/35bed69b-a4f0-419e-9ad8-6bca35fc7281_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GsQ6',
                    name: 'Klang concert series: Rack and Pinion',
                    dateISO: '2023-11-12T14:00:16Z',
                    venue: {
                        name: '',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/cd4/f6cd56c9-99c5-40d3-887f-400def6b8cd4_1545921_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/a/cd4/f6cd56c9-99c5-40d3-887f-400def6b8cd4_1545921_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17Gt_K',
                    name: 'SIBA: Jenna Ristilä - Nainen ja sonaatti',
                    dateISO: '2023-11-14T17:00:00Z',
                    venue: {
                        name: '',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/e48/a1485daa-fa8d-4988-a163-5f3f37132e48_1282981_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/a/e48/a1485daa-fa8d-4988-a163-5f3f37132e48_1282981_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17G0P9',
                    name: 'LAULAJA OLAVI VIRTA',
                    dateISO: '2023-11-15T17:00:00Z',
                    venue: {
                        name: '',
                        city: 'Pori',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/31a/a4ff346f-fc03-4a17-a776-2c6fa5c5331a_960041_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/c/4f2/0109888a-61b5-4525-8432-b026ef04f4f2_105631_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GEag',
                    name: 'OMJ Jazz & Etno: Panu Savolainen & JPP',
                    dateISO: '2023-11-15T17:00:00Z',
                    venue: {
                        name: '',
                        city: 'Oulu',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/333/d4618530-d28d-42ae-a40f-ca7de5ea6333_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/a/333/d4618530-d28d-42ae-a40f-ca7de5ea6333_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GJ8P',
                    name: 'Oulu Sinfonia: Rejoicing Under the Whip',
                    dateISO: '2023-11-16T17:00:00Z',
                    venue: {
                        name: '',
                        city: 'Oulu',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/ebc/c382c09a-7325-48f1-a0fe-3789f64c1ebc_1566241_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/a/ebc/c382c09a-7325-48f1-a0fe-3789f64c1ebc_1566241_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GP8v',
                    name: 'HPO: Fantastic Animals',
                    dateISO: '2023-11-17T17:00:00Z',
                    venue: {
                        name: 'Musiikkitalo, Concert hall',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GYo4',
                    name: 'HKO: Final rehearsals From the New World',
                    dateISO: '2023-11-17T08:00:00Z',
                    venue: {
                        name: 'Musiikkitalo, Concert hall',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17Gw86',
                    name: 'SIBA: Charles Quevillon: Electric Unconscious',
                    dateISO: '2023-11-17T17:00:00Z',
                    venue: {
                        name: '',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/e48/a1485daa-fa8d-4988-a163-5f3f37132e48_1282981_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/a/e48/a1485daa-fa8d-4988-a163-5f3f37132e48_1282981_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GP-A',
                    name: 'Dresdenin orkesterin hehku',
                    dateISO: '2023-11-19T16:00:00Z',
                    venue: {
                        name: '',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/aac/2d140865-a42f-4674-852e-d88f9f2e6aac_1266131_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/a/95e/65aaa72f-3a8e-434a-b34e-602ea0f4695e_496741_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GPuE',
                    name: 'HPO: Northern Lights',
                    dateISO: '2023-11-22T17:00:00Z',
                    venue: {
                        name: 'Musiikkitalo, Concert hall',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GYo9',
                    name: 'HKO: Final rehearsals From the New World',
                    dateISO: '2023-11-22T08:00:00Z',
                    venue: {
                        name: 'Musiikkitalo, Concert hall',
                        city: 'Helsinki',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/c/518/83a05c63-479c-4f7e-a7aa-1932aab77518_105461_TABLET_LANDSCAPE_16_9.jpg',
                },
                {
                    id: 'Z698xZ8KZ17GwoS',
                    name: 'OASBB & Jukka Eskola: Thad Jones 100!',
                    dateISO: '2023-11-22T17:00:00Z',
                    venue: {
                        name: '',
                        city: 'Oulu',
                        country: 'Finland',
                    },
                    thumbnailURL:
                        'https://s1.ticketm.net/dam/a/394/59853b16-d581-49dc-b93e-3af05db53394_1018701_RETINA_PORTRAIT_3_2.jpg',
                    detailsURL:
                        'https://s1.ticketm.net/dam/a/394/59853b16-d581-49dc-b93e-3af05db53394_1018701_TABLET_LANDSCAPE_16_9.jpg',
                },
            ],
            page: {
                size: 20,
                totalElements: 111,
                totalPages: 2,
                number: 1,
            },
        };
    }
}
