import { IEvent, IVenue } from '../models/IEvent';
import { IEventsResult } from '../models/IEventsResult';
import { IGenre } from '../models/IGenre';
import { IAppService, IGetEventsOptions } from './IAppService';

const GENRES_ENDPOINT: string = process.env
    .REACT_APP_GET_GENRES_ENDPOINT as string;
const EVENTS_ENDPOINT: string = process.env
    .REACT_APP_GET_EVENTS_ENDPOINT as string;
const API_KEY: string = process.env.REACT_APP_TICKETMASTER_KEY as string;
const MUSIC_CLASSIFICATION_ID: string = process.env
    .REACT_APP_MUSIC_CLASSIFICATION_ID as string;

export class TicketmasterService implements IAppService {
    /**
     * Returns the response object from the fetch result
     * @param res The fetch result
     */
    private async _getResponse(res: Response): Promise<any> {
        const responseText: string = await res.text();

        //parses the request
        try {
            const response: any = JSON.parse(responseText);

            //for error
            if (!res.ok) {
                throw response;
            }
            //for success
            else {
                return response;
            }
        } catch (e) {
            throw `Could not parse respone: ${responseText}`;
        }
    }
    /**
     * Returns all the available music genres in Ticketmaster
     */
    public async getAllGenres(): Promise<IGenre[]> {
        const options = {
            method: 'get',
            headers: new Headers(),
            body: null,
        };

        const endpoint = `${GENRES_ENDPOINT}?apikey=${API_KEY}`;
        const fetchResult: Response = await fetch(endpoint, options);
        const result = await this._getResponse(fetchResult);

        //remaps the result to something useful
        const ret: IGenre[] = [];
        (result.segment._embedded.genres as any[]).forEach((rawGenre: any) => {
            const genre: IGenre = {
                name: rawGenre.name,
                id: rawGenre.id,
            };

            ret.push(genre);
        });

        return ret;
    }
    /**
     * Retrives the music events from TicketMaster
     * @param options The options of the call
     */
    public async getEvents(options: IGetEventsOptions): Promise<IEventsResult> {
        let reqOptions = {
            method: 'get',
            headers: new Headers(),
            body: null,
        };

        let endpoint = `${EVENTS_ENDPOINT}?countryCode=FI&classificationId=${MUSIC_CLASSIFICATION_ID}&apikey=${API_KEY}`;

        if (options.page) {
            endpoint += `&page=${options.page}`;
        }
        if (options.pageSize) {
            endpoint += `&size=${options.pageSize}`;
        }

        if (options.genreID) {
            endpoint += `&genreId=${options.genreID}`;
        }

        if (options.keyword) {
            endpoint += `&keyword=${options.keyword}`;
        }

        const fetchResult: Response = await fetch(endpoint, reqOptions);
        const result = await this._getResponse(fetchResult);

        const ret: IEventsResult = {
            events: [],
            page: result.page,
        };

        ((result._embedded?.events as any[]) || []).forEach((rawEvent: any) => {
            const rawVenue = rawEvent._embedded.venues[0];
            const venue: IVenue = {
                name: rawVenue.name || '',
                city: rawVenue.city?.name || '',
                country: rawVenue.country?.name || '',
            };

            const event: IEvent = {
                id: rawEvent.id,
                name: rawEvent.name,
                dateISO: rawEvent.dates.start.dateTime,
                venue,
                thumbnailURL:
                    (rawEvent.images as any[]).find(
                        (image) => image.ratio === '3_2' && image.width === 640
                    )?.url || '',
                detailsURL:
                    (rawEvent.images as any[]).find(
                        (image) =>
                            image.ratio === '16_9' && image.width === 1024
                    )?.url || '',
            };

            ret.events.push(event);
        });

        return ret;
    }
}
