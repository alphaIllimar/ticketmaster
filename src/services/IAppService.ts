import { IEventsResult } from '../models/IEventsResult';
import { IGenre } from '../models/IGenre';

export interface IAppService {
    /**
     * Returns all the available music genres in Ticketmaster
     */
    getAllGenres(): Promise<IGenre[]>;
    /**
     * Retrives the music events from TicketMaster
     * @param options The options of the call
     */
    getEvents(options: IGetEventsOptions): Promise<IEventsResult>;
}
/**
 * Describes the options to get events
 */
export interface IGetEventsOptions {
    /**
     * The selected genre ID or NULL for all
     */
    genreID?: string | null;
    /**
     * The search phrased used for the call
     */
    keyword?: string;
    /**
     * Allows you to set the page size of the call
     */
    pageSize?: number;
    /**
     * Allows you to set the requested page
     */
    page?: number;
}
