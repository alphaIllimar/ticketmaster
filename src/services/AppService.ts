import { IAppService } from './IAppService';
import { TicketmasterService } from './TicketmasterService';

/**
 * The service that the app uses
 */
export let AppService: IAppService = new TicketmasterService();
/**
 * Sets the app with a new service
 * @param service The service to be used in the app
 */
export function setAppService(service: IAppService): void {
    AppService = service;
}
